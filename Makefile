all: linkedlist helloworld

linkedlist: 
	@mkdir -p LinkedList/bin
	@gcc -o LinkedList/bin/ListNode.o -c LinkedList/src/ListNode.c
	@gcc -o LinkedList/bin/main.o -c LinkedList/src/main.c
	@gcc -o LinkedList/bin/linkedlist LinkedList/bin/*.o  

helloworld:
	@mkdir -p HelloWorld/bin
	@gcc -o HelloWorld/bin/helloworld HelloWorld/src/main.c

run:
	@echo "Running code..."
	./LinkedList/bin/linkedlist
	./HelloWorld/bin/helloworld

clean: 
	@rm -rf */bin/
