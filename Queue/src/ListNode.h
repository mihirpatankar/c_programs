/*
 * ListNode.h
 *
 *  Created on: Apr 16, 2018
 *      Author: Mihir
 */

#ifndef LISTNODE_H_
#define LISTNODE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * Definition of each node in the list
 */
typedef struct ListNode{
	int val;
	struct ListNode* next;
}ListNode;

#endif /* LISTNODE_H_ */
