/*
 * Queue.h
 *
 *  Created on: Apr 16, 2018
 *      Author: Mihir
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include "ListNode.h"

/*
 * Data structure
 */
typedef struct Queue{
	ListNode* head;
	ListNode* tail;
	int queueSize;
}Queue;

/*
 * Function declarations
 */
void createQueue(Queue* q);
void push(Queue* q,int data);
int front(Queue* q);
void pop(Queue* q);
int size(Queue* q);
void printQueue(Queue* q);
void deleteQueue(Queue* q);

#endif /* QUEUE_H_ */
