/*
 * Queue.c
 *
 *  Created on: Apr 16, 2018
 *      Author: Mihir
 */

#include "Queue.h"

/*
 * Creates a new queue
 */
void createQueue(Queue* q){
	q->head=NULL;
	q->tail=NULL;
	q->queueSize=0;
}

/*
 * Push data to tail of queue
 */
void push(Queue* q,int data){
	q->queueSize++;
	ListNode* newNode = (ListNode*)malloc(sizeof(ListNode));
	newNode->val=data;
	newNode->next=NULL;
	if(q->tail==NULL){
		q->tail=newNode;
		q->head=newNode;
		return;
	}
	q->tail->next=newNode;
	q->tail = q->tail->next;

}

/*
 * Get data from front of queue
 */
int front(Queue* q){
	if(q->head==NULL){
		return -1;
	}
	return q->head->val;
}

/*
 * Deletes the node at front of queue
 */
void pop(Queue* q){
	if(q->head==NULL){
		return;
	}
	ListNode* temp = q->head;
	q->head=q->head->next;
	free(temp);
}

/*
 * Returns size of queue
 */
int size(Queue* q){
	return q->queueSize;
}

/*
 * Prints the entire queue
 */
void printQueue(Queue* q){
	if(q->head==NULL){
		return;
	}
	ListNode* temp = q->head;
	while(temp!=NULL){
		printf("%d ",temp->val);
		temp=temp->next;
	}
	printf("\n");
}
/*
 * Deletes the queue
 */
void deleteQueue(Queue* q){
	if(q->head==NULL){
		return;
	}
	ListNode* ptr=q->head;
	ListNode* temp;
	while(ptr!=NULL){
		temp=ptr;
		ptr=ptr->next;
		free(temp);
	}
}
