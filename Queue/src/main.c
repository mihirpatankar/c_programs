/*
 * main.c
 *
 *  Created on: Apr 19, 2018
 *      Author: Mihir
 */

#include "Queue.h"

int main(){
	Queue q;
	createQueue(&q);
	push(&q,1);
	push(&q,5);
	push(&q,6);
	push(&q,2);
	push(&q,9);
	push(&q,4);
	printf("Size of queue : %d",size(&q));
	printQueue(&q);
	pop(&q);
	pop(&q);
	printQueue(&q);
	printf("Front of queue : %d",front(&q));
	deleteQueue(&q);
	getchar();
	return 0;
}
