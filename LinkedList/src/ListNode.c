/*
 * ListNode.c
 *
 *  Created on: Apr 15, 2018
 *      Author: Mihir
 */

#include "ListNode.h"

/*
 * Append node at the tail of the list
 */
ListNode* appendNode(ListNode* root, int data){
	if(root==NULL){
		root=(ListNode*)malloc(sizeof(ListNode));
		root->val=data;
		root->next=NULL;
		return root;
	}

	ListNode* temp=root;
	while(temp->next!=NULL){
		temp=temp->next;
	}
	ListNode* newNode = (ListNode*)malloc(sizeof(ListNode));
	newNode->val=data;
	newNode->next=NULL;
	temp->next=newNode;

	return root;
}

/*
 * Prepend node at head of the list
 */
ListNode* prependNode(ListNode* root,int data){
	if(root==NULL){
		root=(ListNode*)malloc(sizeof( ListNode));
		root->val=data;
		root->next=NULL;
		return root;
	}
	ListNode* newNode = ( ListNode*)malloc(sizeof( ListNode));
	newNode->val=data;
	newNode->next=root;
	root=newNode;
	return root;
}


/*
 *	Print the entire linked list
 */
void printList( ListNode* root){
	while(root!=NULL){
		printf("%d --> ",root->val);
		root=root->next;
	}
	printf(" NULL \n");
}

/*
 * Reverse linked list
 */
ListNode* reverse(ListNode* root){
	if(root==NULL){
		return root;
	}

	ListNode* prev=NULL;
	ListNode* curr=root;
	ListNode* temp=root;

	while(curr!=NULL){
		temp=curr->next;
		curr->next=prev;
		prev=curr;
		curr=temp;
	}

	return prev;
}

/*
 * Check if linked list has a cycle
 */
bool hasCycle(ListNode* root){
	if(root==NULL){
		return false;
	}
	ListNode* slow=root;
	ListNode* fast=root;

	while(fast->next !=NULL && fast->next->next != NULL){
		slow=slow->next;
		fast=fast->next->next;

		if(slow==fast){
			return true;
		}
	}
	return false;
}

/*
 * Find length of linked list : Recursive and iterative
 */
int lengthIterative(ListNode* root){
	int length=0;
	if(root!=NULL){
		ListNode* temp=root;
		while(temp!=NULL){
			length++;
			temp=temp->next;
		}
	}
	return length;
}

int lengthRecursive(ListNode* root){
	//base condition
	if(root==NULL){
		return 0;
	}

	//recursive condition
	return 1+lengthRecursive(root->next);
}

