/*
 * ListNode.h
 *
 *  Created on: Apr 15, 2018
 *      Author: Mihir
 */

#ifndef LISTNODE_H_
#define LISTNODE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * Definition of each node in the list
 */
typedef struct ListNode{
	int val;
	struct ListNode* next;
}ListNode;

/*
 * Function definitions
 */
ListNode* appendNode(ListNode* root, int data);
ListNode* prependNode(ListNode* root,int data);
void printList( ListNode* root);
ListNode* reverse(ListNode* root);
bool hasCycle(ListNode* root);
int lengthIterative(ListNode* root);
int lengthRecursive(ListNode* root);

#endif /* LISTNODE_H_ */
