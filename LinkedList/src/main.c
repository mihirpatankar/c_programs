/*
 * main.c
 *
 *  Created on: Apr 15, 2018
 *      Author: Mihir
 */

#include "ListNode.h"

int main(){
	ListNode* root=NULL;
	printList(root);
	root=appendNode(root,5);
	root=appendNode(root,4);
	root=appendNode(root,3);
	root=appendNode(root,2);
	root=appendNode(root,1);
	printList(root);
	printf("Length of LL : %d\n",lengthIterative(root));
	printf("Length of LL : %d\n",lengthRecursive(root));
	root=prependNode(root,5);
	root=prependNode(root,4);
	root=prependNode(root,3);
	root=prependNode(root,2);
	root=prependNode(root,1);
	printList(root);
	printf("Length of LL : %d\n",lengthIterative(root));
	printf("Length of LL : %d\n",lengthRecursive(root));
	return 0;
}


